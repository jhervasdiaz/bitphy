#!/bin/bash

cd frontend &&
npm i &&
ng build --prod &&
cd .. &&
cp -r frontend/dist/frontend/* nginx/web &&
