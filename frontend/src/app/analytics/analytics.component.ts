import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit {

  products: any = [];

  sales: any = [];

  constructor(private _http: HttpClient) {
    console.log(this.sales)
    this._http.get("/api/products")
    .subscribe((apiResponse) => {
      this.products = apiResponse;
    })

   }

  totalSales = 0

  searchProduct(){
    const productID = (<HTMLInputElement>document.querySelector('#exampleFormControlSelect1')).value;
    this._http.get(`/api/products/${productID}`)
    .subscribe((apiResponse: any) => {
      this.sales = apiResponse;
      let totalRevenue = 0;
      apiResponse.forEach((sale) => {
        totalRevenue = totalRevenue + (sale["price"] * sale["quantity"])
      })
      this.totalSales = totalRevenue;
    })
  }

  ngOnInit(): void {
  }

}



