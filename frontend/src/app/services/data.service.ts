import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private _http: HttpClient) { }

  shoppingCart = []

  getProducts(){
	  return this._http.get("/api/products")
  }

  addElementToShoppingCart(product, quantity){
    this.shoppingCart.push({
      "productName": product.name, 
      "productPrice": product.price, 
      "productID": product.ID, 
      "quantity": quantity,
      "type": product.type,
      "total": product.price * quantity
    });
  }

  clearCart(){
    this.shoppingCart = []
  }

  sendTicket(){

    var ticket = this.shoppingCart.map(function(item) { return { "productID": item["productID"], "quantity": parseFloat(item["quantity"])} });

    return this._http.post("/api/tickets/newTicket", {
      "products": ticket
    })
  }


}
