import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service'


@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.scss']
})
export class CatalogoComponent implements OnInit {

  allProducts = [];

  weightedProducts = []

  unitaryProducts = []

  currentProducts = this.allProducts

  filteredPrice = "100"

  filterByPrice(){
      this.showAllProducts();
      this.filteredPrice = (<HTMLInputElement>document.querySelectorAll('#priceFilter')[0]).value;
      console.log("Filtrando " + this.filteredPrice)
      this.currentProducts = this.currentProducts.filter((producto => {
            return producto.price < this.filteredPrice;
      }))
  }

  showAllProducts(){
        this.currentProducts = this.allProducts;
        (<HTMLInputElement>document.querySelector('#exampleRadios1')).checked = true;
  }


  showWeightedProducts(){
        this.currentProducts = this.weightedProducts
  }


  showUnitaryProducts(){
        this.currentProducts = this.unitaryProducts
  }

  isUnitaryProduct(product){
        return product.type === "unitary"
  }

  isWeightedProduct(product){
        return product.type === "weighted"
  }


  constructor(private _data: DataService) {
        this._data.getProducts()
        .subscribe((apiResponse: object[]) => {
                this.allProducts = apiResponse;
                this.unitaryProducts = apiResponse.filter(this.isUnitaryProduct);
                this.weightedProducts = apiResponse.filter(this.isWeightedProduct);
                this.showAllProducts();
        })

  }

  elementsInCart = 0;

  addToCart(product){
        let quantity = product.type === "unitary" ? (<HTMLInputElement>document.querySelector('#unitaryQuantity')).value : (<HTMLInputElement>document.querySelector('#weightedQuantity')).value;
        this._data.addElementToShoppingCart(product, quantity);
        this.elementsInCart++;
  }

  ngOnInit(): void {
  }

}


