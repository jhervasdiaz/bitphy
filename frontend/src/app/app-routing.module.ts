import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CatalogoComponent } from './catalogo/catalogo.component';

import { CarritoComponent } from './carrito/carrito.component';

import { AnalyticsComponent } from './analytics/analytics.component';

const routes: Routes = [
	{path:"", component: CatalogoComponent},
	{path:"carrito", component: CarritoComponent},
	{path:"analytics", component: AnalyticsComponent},
	{path:"**", component: CatalogoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
