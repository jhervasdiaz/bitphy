import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.scss']
})
export class CarritoComponent implements OnInit {

  buyList = this._data.shoppingCart;

  priceFinal = 0;

  status = "waitingConfirm";

  constructor(private _data: DataService, private _router: Router) { 
    let ticketPrice = 0;
    this._data.shoppingCart.forEach((element) => {
      console.log(element.total)
      ticketPrice = ticketPrice + element.total
    })
    this.priceFinal = ticketPrice;
  }

  confirmTicket(){
    this.status = "loading";
    this._data.sendTicket()
    .subscribe((responseAPI) => {
      this.status = "confirmed";
      this._data.clearCart();
      setTimeout(() => {this._router.navigateByUrl("/analytics")}, 3000)
    })
  }

  ngOnInit(): void {
  }

}
