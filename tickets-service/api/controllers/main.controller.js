
const { validationResult } = require('express-validator');
const ticketsModel = require('../models/tickets.model');

//Get single ticket by ID
exports.getTicket = async (req, res) => {

	try {
            const result = await ticketsModel.getTransactionsForTicket(req.params.ticket)
            res.send({ "status": "ok", "data": result })
        } catch (error) {
            res.send(error)
        }

}


//Create ticket and add transactions
exports.createTicket = async (req, res) => {
    const errors = validationResult(req) //Ejecuta las validaciones
    if (errors.isEmpty()) {
        try {
	    // Calculemos la fecha
      	const now  = new Date();
	    const timestamp = now.toISOString().slice(0,10).replace(/-/g,"");

	    //Primero, creamos un ticket vacío
        const emptyTicket = await ticketsModel.createTicket(timestamp)
	    
	    try {

			transactions = req.body.products
			ticketID = emptyTicket.insertId
			//Creamos las transacciones necesarias para este ticket
			const transactionsPromises = []
			transactions.forEach((transaction) => {
				const transPromise = ticketsModel.createTransaction(
					transaction.productID,
					ticketID,
					transaction.quantity
				)
				transactionsPromises.push(transPromise)
			})

			Promise.all(transactionsPromises)
			.then((confirmedTransactions) => {
				// Finalmente, añadimos las nuevas transacciones al ticket

				const savedTransactions = []

				confirmedTransactions.forEach((confirmedTrans) => {
					const confirmedTransProm = ticketsModel.addTransactionToTicket(
						ticketID,
						confirmedTrans.insertId
					)
					savedTransactions.push(confirmedTransProm);
				})

				Promise.all(savedTransactions)
				.then(
					//Hurray!!
					res.send({"status":"ok", "ticketID": ticketID})
				)
				.catch((error) => {console.log(error); res.send({"status":"error", "causa": error})})

			})
			.catch((error) => {console.log(error); res.send({"status":"error", "causa": error})})


			} catch (error) {
				console.log(error); 
				res.send({"status":"error", "causa": error})
			}
	    	    

        } catch (error) {
            console.log(error); 
	    	res.send({"status":"error", "causa": error})
        }

    } else {

	res.status(400).send({"status":"error", "causa": errors})

    }

}
