
const connection = require('./db.model');

//Create one empty ticket
exports.createTicket = (date) => {

    return new Promise(async (resolve, reject) => { 
        try {
            const result = await connection.query(`
		INSERT INTO bitphy.tickets (date, totalPrice)
		VALUES (${date}, 0);
		`)
            resolve(result);
        } catch (error) {
            reject(error);
        }
    })

}

//Create one transaction
exports.createTransaction = (productID, ticketID, quantity) => {

    return new Promise(async (resolve, reject) => { 
        try {
            const result = await connection.query(`
		INSERT INTO bitphy.transactions (products_ID, tickets_ID, quantity) 
		VALUES (${productID}, ${ticketID}, ${quantity});
                `)
            resolve(result);
        } catch (error) {
            reject(error);
        }
    })

}

//Add a given transaction to a given ticket
exports.addTransactionToTicket = (ticketID, transactionID) => {

    return new Promise(async (resolve, reject) => { 
        try {
            const data = await connection.query(`
		UPDATE bitphy.tickets 
		SET totalPrice=(
        		SELECT SUM(pr.price * tr.quantity)
        		FROM bitphy.products pr
        		JOIN bitphy.transactions tr
        		ON pr.ID = tr.products_ID
        		WHERE tr.tickets_ID = ${ticketID}
			)
		WHERE ID = ${transactionID};

	    `)
            resolve(data);
        } catch (error) {
            reject(error);
        }
    })

}

//Get all transactions for a given ticket
exports.getTransactionsForTicket = (ticketID) => {

    return new Promise( async (resolve, reject) => {
        try {
            const data = await connection.query(`
		SELECT
        		pr.name AS 'product',
    			tr.quantity AS 'quantity',
    			pr.price AS 'price',
    			pr.type AS 'type',
        		pr.price * tr.quantity AS 'totalPrice'
		FROM bitphy.transactions tr
		LEFT JOIN bitphy.tickets ti
		ON ti.ID = tr.tickets_ID
		RIGHT JOIN bitphy.products pr
		ON pr.ID = tr.products_ID
		WHERE ti.ID = ${ticketID};
		`)
            resolve(data)
        } catch (error) {
            reject(error)
        }
    })

}

