const express = require('express');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const ticketsController = require('./controllers/main.controller');
const { body } = require('express-validator');

//Creamos el servidor
const server = express();

//Middleware
server.use(helmet());
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

server.get('/api/tickets/:ticket', ticketsController.getTicket);

server.post('/api/tickets/newTicket',
	    [
		body('products').isArray(),
		body('products.*.productID', 'productID debe ser un número').exists().isNumeric(),
		body('products.*.quantity', 'quantity debe ser un número').exists().isNumeric()
	    ],
	    ticketsController.createTicket);

server.listen(8001);
