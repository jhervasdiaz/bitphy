
//Conexión con la bbdd

const mysql = require('mysql');

//WRAPPER para usar promesas
class Database {
    constructor(config){
        this.connection = mysql.createPool(config)
    }
    query(sql){
        return new Promise((resolve, reject) => {
            this.connection.query(sql, (error, result) => {
                if (error){
                    return reject(error)
                } else {
                    resolve(result)
                }
            })
        })
    }
}

//la config debe estar en un archivo a parte (por temas de seguridad)
const connection = new Database({
	host: 	  'mysql',
	user: 	  'default_user',
	password: 'secret'
})

module.exports = connection;
