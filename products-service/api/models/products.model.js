
const connection = require('./db.model');

//Get all products
exports.getAllProducts = () => {
	return new Promise(async (resolve, reject) => {
		try {
			const results = await connection.query(`
				SELECT * FROM bitphy.products;
			`)
			resolve(results)
		} catch (error) {
			reject(error)
		}
	})
}


//Get the total revenue of a product over time
exports.getRevenueByProduct = (productID) => {
	return new Promise(async (resolve, reject) => {
		try {
			const results = await connection.query(`
				SELECT
					pr.name AS 'name',
					pr.price AS 'price',
					tr.quantity,
					ti.date
				FROM bitphy.transactions tr
				LEFT JOIN bitphy.products pr 
				ON tr.products_ID = pr.ID
				LEFT JOIN bitphy.tickets ti
				ON tr.tickets_ID = ti.ID
				WHERE pr.ID = ${productID};
			`)
			resolve(results)
		} catch (error) {
			reject(error)
		}
	})
}
