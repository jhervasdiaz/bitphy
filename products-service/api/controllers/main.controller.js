
const productsModel = require('../models/products.model.js');

//Get all the products
exports.getProducts = async (req, res) => {
	try {
		const results = await productsModel.getAllProducts();
		res.send(results);
	} catch (error) {
		console.log(error);
		res.send({"status":"error", "causa":error})
	}
}

//Get revenue of product over time
exports.getProductRevenue = async (req, res) => {
	try {
		const results = await productsModel.getRevenueByProduct(req.params.productID);
		res.send(results);
	} catch (error) {
		console.log(error);
		res.send({"status":"error", "causa":error})
	}
}
