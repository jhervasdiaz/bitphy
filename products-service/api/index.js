const express = require('express');
const helmet = require('helmet');
const productsController = require('./controllers/main.controller');

//Creamos el servidor
const server = express();

//Middleware
server.use(helmet());

server.get('/api/products', productsController.getProducts);

server.get('/api/products/:productID', productsController.getProductRevenue);



server.listen(8000);
