#!/bin/bash
Errors=0

Green='\033[0;32m'
Red='\033[0;31m'
Color_Off='\033[0m'

Check_Mark='\xE2\x9C\x94'

assert_equals () {
  if [ "$1" = "$2" ]; then
    echo -e "$Green $Check_Mark Success $Color_Off"
  else
    echo -e "$Red Failed $Color_Off"
    echo -e "$Red Expected $1 to equal $2 $Color_Off"
    Errors=$((Errors  + 1))
  fi
}

get_json_value () {
  echo $1 | jq -r $2
}

get_json_array_length () {
  echo $1 | jq ". | length"
}


echo ""
echo "======================================================="
echo ""
echo "### START /api/tickets"
url="localhost/api" 

echo ""
echo " Create a new ticket with /api/tickets/newTicket should return a status of OK"

response=$(curl -H "Content-Type: application/json" -s -X POST "${url}/tickets/newTicket" --data '{"products":[{"productID":1,"quantity":2}]}')
assert_equals "$(get_json_value "$response" ".status")" "ok"

echo ""
echo " Create a new ticket without body on /api/tickets/newTicket should return a status of error"

response=$(curl -H "Content-Type: application/json" -s -X POST "${url}/tickets/newTicket" --data '')
assert_equals "$(get_json_value "$response" ".status")" "error"


echo ""
echo " Create a new ticket with a bad ProductID on /api/tickets/newTicket should return a status of error"

response=$(curl -H "Content-Type: application/json" -s -X POST "${url}/tickets/newTicket" --data '{"products":[{"productID":"test","quantity":2}]}')
assert_equals "$(get_json_value "$response" ".status")" "error"


echo ""
echo " Create a new ticket with a bad quantity on /api/tickets/newTicket should return a status of error"

response=$(curl -H "Content-Type: application/json" -s -X POST "${url}/tickets/newTicket" --data '{"products":[{"productID":1,"quantity":"test"}]}')
assert_equals "$(get_json_value "$response" ".status")" "error"


echo ""
echo " Get a ticket on /api/tickets/1 should return a status of ok"
response=$(curl -s "${url}/tickets/1")
assert_equals "$(get_json_value "$response" ".status")" "ok"


echo ""
echo " Get a ticket on /api/tickets/9999 should return an empty array (no data)"
response=$(curl -s "${url}/tickets/9999")
assert_equals "$(get_json_value "$response" ".data")" "[]"


echo ""
echo "### END /api/tickets"
echo ""

echo "========================================================="

echo ""
echo "### START /api/products"
url="localhost/api" 

echo ""
echo " /api/products should return an array with the 10 dummy results we created"

response=$(curl -s "${url}/products")
assert_equals "$(get_json_array_length "$response")" "10"


echo ""
echo " /api/products/1 should return all the transactions from the product 'rice'"

response=$(curl -s "${url}/products/1")
assert_equals "$(get_json_value "$response" ".[0].name")" "arroz"



echo ""
echo "### END /api/products"
echo ""
if [ "$Errors" -gt "0" ]; then
  exit 1
else
  exit 0
fi
