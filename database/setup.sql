
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema bitphy
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema bitphy
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bitphy` DEFAULT CHARACTER SET latin1 ;
USE `bitphy` ;

-- -----------------------------------------------------
-- Table `bitphy`.`products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bitphy`.`products` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `type` ENUM("unitary", "weighted") NOT NULL,
  `price` FLOAT NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `ID_UNIQUE` (`ID` ASC) VISIBLE,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bitphy`.`tickets`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bitphy`.`tickets` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `date` TIMESTAMP NOT NULL,
  `totalPrice` FLOAT UNSIGNED NOT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bitphy`.`transactions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bitphy`.`transactions` (
  `quantity` FLOAT NOT NULL,
  `ID` INT NOT NULL AUTO_INCREMENT,
  `products_ID` INT NOT NULL,
  `tickets_ID` INT NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `ID_UNIQUE` (`ID` ASC) VISIBLE,
  INDEX `fk_transactions_products_idx` (`products_ID` ASC) VISIBLE,
  INDEX `fk_transactions_tickets1_idx` (`tickets_ID` ASC) VISIBLE,
  CONSTRAINT `fk_transactions_products`
    FOREIGN KEY (`products_ID`)
    REFERENCES `bitphy`.`products` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_transactions_tickets1`
    FOREIGN KEY (`tickets_ID`)
    REFERENCES `bitphy`.`tickets` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- ------------------------------------------------------
-- Dummy data to start working
-- ------------------------------------------------------

INSERT INTO bitphy.products (name, type, price)
VALUES ("arroz", "weighted", 1.00);

INSERT INTO bitphy.products (name, type, price)
VALUES ("tomates", "weighted", 3.00);

INSERT INTO bitphy.products (name, type, price)
VALUES ("naranjas", "weighted", 2.00);

INSERT INTO bitphy.products (name, type, price)
VALUES ("patatas", "weighted", 1.00);

INSERT INTO bitphy.products (name, type, price)
VALUES ("barra de pan", "unitary", 1.00);

INSERT INTO bitphy.products (name, type, price)
VALUES ("docena de huevos", "unitary", 4.00);

INSERT INTO bitphy.products (name, type, price)
VALUES ("botella de agua", "unitary", 1.00);

INSERT INTO bitphy.products (name, type, price)
VALUES ("sangre de unicornio", "weighted", 6.00);

INSERT INTO bitphy.products (name, type, price)
VALUES ("botella de poción de mana", "unitary", 4.00);

INSERT INTO bitphy.products (name, type, price)
VALUES ("espada legendaria", "unitary", 7.00);
