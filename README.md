
# Bitphy challenge

[![Standard - JavaScript Style Guide](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com/)

This project is an example of a shop billing platform using microservices, REST APIs, NodeJS, MySQL, Nginx, and Angular 2+.

![alt text](https://d2eip9sf3oo6c2.cloudfront.net/series/square_covers/000/000/120/full/EGH_NodeDocker_1000.png)

Full documentation of this platform [is on its Confluence page.](https://joseignaciohervasdiaz.atlassian.net/l/c/7bPPz8UB)

---
### How to use?

```bash
$ git clone https://jhervasdiaz@bitbucket.org/jhervasdiaz/bitphy.git
$ cd bitphy
$ docker-compose up -d --build mysql products tickets nginx
```

After this all the containers should be up and running their respectives microservices. You can `docker-compose ps` to make sure everything is running correctly.

**NOTE:** you will need to have root permissions in order for NPM to install the dependencies on the microservices. If you find problems on the `products` and `tickets` containers, try running compose with sudo.

Go to http://localhost to see the web interface!

---
### Which API Endpoints can I use?

[The API endpoints are fully docucmented on this link.](https://documenter.getpostman.com/view/11027146/SzYgQEn6?version=latest)

---
### How to test?

When created, the database container is loaded with **10 dummy data records** in order to test the platform.

With the help of a bash script we can make a quick end to end test against the API:

```bash
$ cd bitphy
$ ./integration/test.sh
```

Obviously in the future we will need to implement more complex tests, and use unit testing frameworks like mocha & chai.

---
### Can I re-build the front-end interface?

Of course you can! You only will need npm@6 and node@12

If you have them installed, just run the script located on the frontend folder:

```bash 
$ cd frontend
$ ./build-frontend.sh
```

---
### Is there any live demo?

Of course!! :D 

I have deployed this project on an EC2 instance with an Elastic IP associated:

http://3.20.3.145/